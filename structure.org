#+TITLE: STRUCTURE FOR POLYNOMIAL ARITHMETIC USING LINKED LIST
#+AUTHOR: VLEAD
#+DATE: [2019-11-12 Tue]
#+SETUPFILE: org-templates/level-1.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: \n:t

* Introduction
  This document captures the structure for the polynomial arithmetic using linked list experiment.

* Experiment Structure
  Experiment consists of a preamble with a set of artefacts,
  where the overview of the overall experiment is
  defined. Then it is followed by a learning units. Each
  learning unit consists of a mandatory preamble with a set
  of artefacts, which describes it. Then each learning unit
  is followed by a set of tasks and quizzes. Each task is
  followed by a set of artefacts.

** =EXP : Polynomial arithmetic using Linked List=
*** LEARNING OBJECTIVES OF THIS EXPERIMENT
    + Given two polynomial arithmetic equations.
    + Demonstrate knowledge of time complexity of Polynomial arithmetic using Linked Lists.
    + Perform addition and subtraction using Polynomial arithmetic using Linked Lists

*** DESCRIPTION OF VIDEO
    + Welcome the user to experiment
    + Overview of the experiment & aims
    + Flow of the experiment and tips (recap before taking
      pre-test, go through LU videos, do the practice
      exercises properly etc)

*** Preamble
    + *Text Artefact*  : Estimated time 
    + *Video Artefact* : Introduction to module and tips for user.
    + *Text Artefact*  : Prerequisites
    + *Text Artefact*  : Learning objectives
    + *Text Artefact*  : Weightage of modules

** =LU 1: Pre Test=
*** LEARNING OBJECTIVES OF THIS LEARNING UNIT
    + Test knowledge on linked list, time complexity and space
      complexity.
    + Test knowledge of Linked Lists and Polynomial expressions.

*** Preamble
    + *Text Artefact* : Estimated Time
    + *Text Artefact* : Instructions for Pre-Test

*** Task : Recap
    + *Text Artefact*  : What is Linked List?
    + *Image Artefact* : Image showing snapshot of the experiment
    + *Text Artefact*  : Time and Space Complexity

*** Quiz : Pre-test Quiz
    It has simple multiple choice questions quiz to let the
    user self test his/her understanding on "Recap" task.

** =LU 2: Polynomial arithmetic using Linked Lists=
*** LEARNING OBJECTIVES OF THIS LEARNING UNIT
    + Gain the intuition behind Polynomial expressions
    + Watch and visualise the Polynomial arithmetic using Linked Lists algorithm.

*** Preamble
    + *Text Artefact*  : Estimated Time
    + *Video Artefact* : Polynomial arithmetic using Linked Lists Concept
    + *Text Artefact*  : Learning objectives 

*** Task : Concept and Strategy (Remembering, Understanding)
    + *Text Artefact*  : How can we perform Polynomial arithmetic expressions?
    + *Image Artefact* : Intuition Behind the Algorithm
    + *Text Artefact*  : Polynomial arithmetic using Linked Lists Concept  
    + *Image Artefact* : Polynomial arithmetic using Linked Lists Algorithm
    + *Text Artefact*  : Consolidated Algorithm for Polynomial arithmetic using Linked Lists 

*** Task : Demo (Visualising, Understanding)
    + *Demo artefact* : will contain play/pause button, user
      can generate random set of equations and watch them get
      added/ subtracted using Linked List. User can adjust speed
      using a slider and can click on "next step" manually
      to see the steps at his/her own pace.

*** Task : Practice (Applying)
    + *Practice artefact* : Similar to demo artefact. Here,
      user has the option to "add/ subtract the numbers", or move
      on to the "next" elements. User receieves a step by
      step feedback. If his/her step is wrong, a box will
      show feedback and give them the chance to try
      again. Wrong move will not be made in this case.

*** Task : Exercise (Applying)
    + *Exercise artefact* : Solve questions related to
      Polynomial arithmetic using Linked Lists using artefact. Feedback will only be
      given at the end, when the user submits their answer.

*** Quiz : Quiz (Applying)
    + Questions to test basic understanding 

** =LU 3: Analysis=
*** LEARNING OBJECTIVES OF THIS LEARNING UNIT
    + Time and Space Complexity : We will learn about the
      running time of one iteration, and then extend it to O(m + n) (where m and n are number of nodes in first and second lists respectively) time complexity of polynomial arithmetic.
    
*** Preamble
    + *Text Artefact*  : Estimated Time
    + *Video Artefact* : Polynomial arithmetic using Linked Lists Concept
    + *Text Artefact*  : Learning objectives

*** Task : Time and space complexity (Remembering and Understanding)
    + *Text Artefact* : Running Time of Polynomial arithmetic using Linked Lists Concept
    + *Demo artefact* : Artefact that shows how comparisions are made to add/ subtract the polynomial equations.
    + *Text Artefact* : Space Complexity of Polynomial arithmetic using Linked Lists Concept

*** Task : Analysis Quiz (Applying)
    + Questions to test basic understanding 

** =LU 4: Post Test Quiz=
*** LEARNING OBJECTIVES OF THIS LEARNING UNIT
    + Test user knowledge by application based questions.
    + Give single and multi correct questions.

*** Preamble
    + *Text Artefact* : Estimated Time
    + *Text Artefact* : Instructions for Quiz

*** Task : Post-Test Quiz (Applying, Testing)
    + Questions to test his/her understanding on overall
      experiment

** =LU 5: Further Readings=
*** Preamble
    + *Text Artefact* : Useful links for Coding
      implementation, Visualizations and online mcq quizzes

